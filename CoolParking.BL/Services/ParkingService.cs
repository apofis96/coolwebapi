﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : CoolParking.BL.Interfaces.IParkingService
    {
        Interfaces.ITimerService withdrawTimer;
        Interfaces.ITimerService logTimer;
        Interfaces.ILogService logService;
        Parking parking;
        List<TransactionInfo> transactions = new List<TransactionInfo>();
        private bool disposed = false;
        public decimal CurrentSum { get; private set; }
        public ParkingService(Interfaces.ITimerService withdrawTimer, Interfaces.ITimerService logTimer, Interfaces.ILogService logService)
        {
            CurrentSum = 0M;
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            this.logService = logService;
            parking = Parking.Instance;
            this.withdrawTimer.Elapsed += MakePayment;
            this.withdrawTimer.Start();
            this.logTimer.Elapsed += WriteToLog;
            this.logTimer.Start();
        }
        public decimal GetBalance()
        {
            return parking.Balance;
        }
        public int GetCapacity()
        {
            return parking.Capacity;
        }
        public int GetFreePlaces()
        {
            return GetCapacity() - parking.Vehicles.Count;
        }
        public int GetOccupiedPlaces()
        {
            return parking.Vehicles.Count;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() > 0)
            {
                if(GetOccupiedPlaces()==0 || SelectVehicle(vehicle.Id) == null)
                    parking.Vehicles.Add(vehicle);
                else
                    throw new ArgumentException("Vehicle with this Id already parked");
            }
            else
            {
                throw new InvalidOperationException("Parking is full");
            }
        }
        public void RemoveVehicle(string vehicleId)
        {
            if (parking.Vehicles.Count == 0)
            {
                throw new InvalidOperationException("Parking is empty");
            }
            Vehicle selectedVehicle = SelectVehicle(vehicleId);
            if (selectedVehicle == null)
                throw new ArgumentException("Vehicle with this Id not parked");
            if (selectedVehicle.Balance < 0M)
            {
                throw new InvalidOperationException("Cannot remove vehicle with negative balance");
            }
            parking.Vehicles.Remove(selectedVehicle);
            return;
        }
        private Vehicle SelectVehicle(string vehicleId)
        {
            var selectedVehicles = from vehicle in parking.Vehicles
                                   where vehicle.Id == vehicleId
                                   select vehicle;
            if (selectedVehicles.Count() == 0)
                return null;
            return selectedVehicles.First();
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (parking.Vehicles.Count == 0)
            {
                throw new InvalidOperationException("Parking is empty");
            }
            if (sum < 0M)
            {
                throw new ArgumentException("Sum cannot be negative");
            }
            Vehicle selectedVehicle = SelectVehicle(vehicleId);
            if (selectedVehicle == null)
                throw new ArgumentException("Vehicle with this Id not parked");
            selectedVehicle.Balance += sum;
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactions.ToArray();
        }
        public string ReadFromLog()
        {
            return logService.Read();
        }
        private void MakePayment(object source, System.Timers.ElapsedEventArgs e)
        {
            decimal diffBalance, tmpTarrif, penalty = Settings.penaltyRatio;
            foreach (Vehicle parkedVehicle in parking.Vehicles)
            {
                diffBalance = parkedVehicle.Balance;
                tmpTarrif = Settings.tariffs[parkedVehicle.VehicleType];
                if (parkedVehicle.Balance >= tmpTarrif)
                {
                    parkedVehicle.Balance -= tmpTarrif;
                }
                else 
                {
                    if (parkedVehicle.Balance > 0M)
                    {
                        tmpTarrif -= parkedVehicle.Balance;
                        parkedVehicle.Balance = 0M;
                    }
                    parkedVehicle.Balance -= tmpTarrif * penalty;
                }
                diffBalance -= parkedVehicle.Balance;
                CurrentSum += diffBalance;
                parking.Balance += diffBalance;
                transactions.Add(new TransactionInfo(diffBalance, parkedVehicle.Id));
            }
        }
        private void WriteToLog(object source, System.Timers.ElapsedEventArgs e)
        {
            string str = DateTime.Now.ToString("MM/dd/yyyy h:mm:ss tt") + ": " + "Earned for the period: " + CurrentSum.ToString()+"\n";
            if (transactions.Count != 0)
            {
                foreach (TransactionInfo transaction in transactions)
                {
                    str += transaction.ToString();
                }
                transactions.Clear();
            }
            CurrentSum = 0M;
            logService.Write(str);
        }
        public void Dispose()
        {
            logTimer.Dispose();
            logTimer = null;
            withdrawTimer.Dispose();
            withdrawTimer = null;
            parking.Vehicles.Clear();
            parking.Balance = Settings.parkingInitialBalance;
            GC.SuppressFinalize(this);
        }
    }
}