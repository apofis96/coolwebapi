﻿using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : CoolParking.BL.Interfaces.ITimerService
    {
        private Timer Timer;
        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }

        public TimerService(double interval)
        {
            Interval = interval;
        }
        public void Start()
        {
            Timer = new Timer(Interval*1000d);
            Timer.Elapsed += OnElapsed;
            Timer.Start();
        }
        public void Stop() 
        {
            Timer?.Stop();
        }
        private void OnElapsed(object source, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(this, e);
        }
        public void Dispose()
        {
            Stop();
            GC.SuppressFinalize(this);
            Timer?.Dispose();
            Timer = null;
        }
    }
}