﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        static private Regex regex = new Regex(@"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$", RegexOptions.Singleline);
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if(balance < 0M)
            {
                throw new ArgumentException("Negative balance");
            }
            id = id.Trim();
            if(!ValidateRegistrationPlateNumber(id))
            {
                throw new ArgumentException("Id wrong format");
            }
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random r = new Random();
            string randId = GenerateAlef() + "-";
            for (int i = 0; i < 4; i++)
            {
                randId += r.Next(0, 9);
            }
            randId += "-" + GenerateAlef();
            return randId;
            string GenerateAlef()
            {
                string str = "";
                for (int i = 0; i < 2; i++)
                {
                    str+=Convert.ToChar(r.Next(65, 90)).ToString();
                }
                return str;
            }

        }
        public static bool ValidateRegistrationPlateNumber(string plate)
        {
            return regex.IsMatch(plate);
        }
        public override string ToString()
        {
            return Id + " -=- " + VehicleType + " -=- " + "Balance: " + Balance+"\n";
        }
    }
}