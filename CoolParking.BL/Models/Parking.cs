﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    internal class Parking
    {
        private static readonly Lazy<Parking> instance = new Lazy<Parking>(() => new Parking());
        public static Parking Instance
        {
            get
            {
                return instance.Value;
            }
        }
        internal decimal Balance { get; set; }
        internal int Capacity { get; private set; }
        internal List<Vehicle> Vehicles = new List<Vehicle>();
        private Parking()
        {
            Balance = Settings.parkingInitialBalance;
            Capacity = Settings.parkingCapacity;
        }
    }
}