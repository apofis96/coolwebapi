﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; }
        public DateTime Date { get; }
        public string VehicalId { get; }
        public TransactionInfo(decimal sum, string vehicleId)
        {
            Sum = sum;
            VehicalId = vehicleId;
            Date = DateTime.Now;
        }
        public override string ToString()
        {
            return Date.ToString("MM/dd/yyyy h:mm:ss tt") + ": " + Sum + " money withdrawn from vehicle with Id='" + VehicalId +"'\n";
        }
    }
}