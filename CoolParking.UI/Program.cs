﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using System.Globalization;
using System.Text;
using CoolParking.UI.Models;

namespace CoolParking.UI
{
    class Program
    {
        static readonly HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            try
            {
                client.BaseAddress = new Uri("https://localhost:5001/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                RunAsync().GetAwaiter().GetResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        static async Task RunAsync()
        {
            string menuItems = @"1 : Display Parking balance.
2 : Display available/occupied parking places.
3 : Display Transactions for current period.
4 : Display the transaction history.
5 : Display Vehicles in the parking lot.
6 : Put Vehicle in the parking lot.
7 : Pick up Vehicle from the parking lot.
8 : Top up the balance of Vehicle.
0 : Exit.
h : Display this menu.";
            Console.WriteLine(menuItems);
            bool loop = true;
            while (loop)
            {
                Console.Write("Main menu input: ");
                char menu = Console.ReadKey().KeyChar;
                Console.WriteLine("\n================================");
                switch (menu)
                {
                    case '1':
                        decimal val = await GetBalance();
                        Console.WriteLine($"Parking balance: {val.ToString()}.");
                        break;
                    case '2':
                        int free = await GetFreePlaces();
                        int occupied = await GetCapacity() - free;
                        Console.WriteLine($"Parking places: {free} available, {occupied} occupied.");
                        break;
                    case '3':
                        Console.WriteLine("Transactions:");
                        foreach (Transaction tr in await GetLastParkingTransactions())
                        {
                            Console.WriteLine(tr.ToString());
                        }
                        break;
                    case '4':
                        try
                        {
                            Console.WriteLine($"Transaction history:\n" + await ReadFromLog());
                        }
                        catch (InvalidOperationException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case '5':
                        Console.WriteLine("Vehicles:");
                        foreach (Vehicle vh in await GetVehicles())
                        {
                            Console.WriteLine(vh.ToString());
                        }
                        break;
                    case '6':
                        await PutVehicle();
                        break;
                    case '7':
                        await PickVehicle();
                        break;
                    case '8':
                        await TopVehicle();
                        break;
                    case '0':
                        loop = false;
                        break;
                    case 'h':
                    default:
                        Console.WriteLine(menuItems);
                        break;
                }
            }
        }
        static async Task<decimal> GetBalance()
        {
            HttpResponseMessage response = await client.GetAsync("api/parking/balance");
            response.EnsureSuccessStatusCode();
            return Convert.ToDecimal(await response.Content.ReadAsStringAsync(), CultureInfo.InvariantCulture);
        }
        static async Task<int> GetFreePlaces()
        {
            HttpResponseMessage response = await client.GetAsync("api/parking/freePlaces");
            response.EnsureSuccessStatusCode();
            return Convert.ToInt32(await response.Content.ReadAsStringAsync(), CultureInfo.InvariantCulture);
        }
        static async Task<int> GetCapacity()
        {
            HttpResponseMessage response = await client.GetAsync("api/parking/capacity");
            response.EnsureSuccessStatusCode();
            return Convert.ToInt32(await response.Content.ReadAsStringAsync(), CultureInfo.InvariantCulture);
        }
        static async Task<List<Transaction>> GetLastParkingTransactions()
        {
            HttpResponseMessage response = await client.GetAsync("api/transactions/last");
            response.EnsureSuccessStatusCode();
            return JsonSerializer.Deserialize<List<Transaction>>(await response.Content.ReadAsStringAsync());
        }
        static async Task<string> ReadFromLog()
        {
            HttpResponseMessage response = await client.GetAsync("api/transactions/all");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                await PrintError(response);
                return "";
            }
            return JsonSerializer.Deserialize<string>(await response.Content.ReadAsStringAsync());
        }
        static async Task<List<Vehicle>> GetVehicles()
        {
            HttpResponseMessage response = await client.GetAsync("api/vehicles");
            response.EnsureSuccessStatusCode();
            return JsonSerializer.Deserialize<List<Vehicle>>(await response.Content.ReadAsStringAsync());
        }
        static async Task PutVehicle()
        {
            string plate;
            int type;
            decimal balance;
            Console.Write("Adding Vehicle:\nLicense plate:\n0 : Random\n1 : Manual\ne : Exit\nInput:");
            char menu = Console.ReadKey().KeyChar;
            Console.WriteLine();
            switch (menu)
            {
                case '1':
                    Console.Write("Input license plate (format AA-0000-AA): ");
                    plate = Console.ReadLine();
                    break;
                case 'e':
                    return;
                case '0':
                default:
                    plate = Vehicle.GenerateRandomRegistrationPlateNumber();
                    break;
            }
            Console.WriteLine("License plate: " + plate);
            Console.WriteLine("Chose Vehicle type:");
            int types = -1;
            foreach (var item in Enum.GetValues(typeof(VehicleTypes)))
            {
                types++;
                Console.WriteLine($"{(int)item} : {item}.");
            }
            do
            {
                Console.Write("Input: ");
                string input = Console.ReadLine();
                type = int.TryParse(input, out type) ? type : -1;
                if (type == -1 || type < 0 || type > types)
                {
                    Console.WriteLine("Wrong input");
                }
                else
                    break;
            }
            while (true);
            do
            {
                Console.WriteLine("Vehicle balance: ");
                balance = decimal.TryParse(Console.ReadLine(), out balance) ? balance : -1;
                if (balance < 0)
                {
                    Console.WriteLine("Wrong input, balance must be positive number");
                }
                else
                    break;
            }
            while (true);
            Vehicle vh = new Vehicle(plate, type, balance);
            HttpResponseMessage response = await client.PostAsync("api/vehicles", new StringContent(JsonSerializer.Serialize(vh), Encoding.UTF8, "application/json"));
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                await PrintError(response);
                return;
            }
            Console.WriteLine(JsonSerializer.Deserialize<Vehicle>(await response.Content.ReadAsStringAsync()));
            Console.WriteLine("Done");
        }
        static async Task PickVehicle()
        {
            Console.WriteLine("Pick up Vehicle");
            Vehicle vh = await SelectVehicle();
            if (vh == null)
                return;
            Console.WriteLine(vh.ToString());
            HttpResponseMessage response = await client.DeleteAsync("api/vehicles/" + vh.Id);
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                await PrintError(response);
                return;
            }
            Console.WriteLine("Done");
        }
        static async Task TopVehicle()
        {
            Console.WriteLine("Top up Vehicle");
            try
            {
                Vehicle vh = await SelectVehicle();
                if (vh == null)
                    return;
                Console.WriteLine(vh.ToString());
                decimal input;
                do
                {
                    Console.Write("Input emount: ");
                    input = decimal.TryParse(Console.ReadLine(), out input) ? input : -1;
                    if (input < 0)
                    {
                        Console.WriteLine("Wrong input");
                    }
                    else
                        break;
                }
                while (true);
                TopUpVehicle topUpVehicle = new TopUpVehicle(vh.Id, input);
                HttpResponseMessage response = await client.PutAsync("api/transactions/topUpVehicle", new StringContent(JsonSerializer.Serialize(topUpVehicle), Encoding.UTF8, "application/json"));
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    await PrintError(response);
                    return;
                }
                Console.WriteLine(JsonSerializer.Deserialize<Vehicle>(await response.Content.ReadAsStringAsync()));
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Done");
        }
        static async Task<Vehicle> SelectVehicle()
        {
            Console.Write("Input license plate (format AA-0000-AA): ");
            string plate = Console.ReadLine();
            HttpResponseMessage response = await client.GetAsync("api/vehicles/" + plate);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                await PrintError(response);
                return null;
            }
            return JsonSerializer.Deserialize<Vehicle>(await response.Content.ReadAsStringAsync());
        }
        static async Task PrintError(HttpResponseMessage response)
        {
            Console.WriteLine($"Error: {response.StatusCode}");
            foreach (KeyValuePair<string, string> kvp in JsonSerializer.Deserialize<Dictionary<string, string>>(await response.Content.ReadAsStringAsync()))
            {
                Console.WriteLine($"{kvp.Key} : {kvp.Value}");
            }
        }
    }
}
