﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CoolParking.UI.Models
{
    public class Transaction
    {

        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; set; }
        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }
        [JsonPropertyName("transactionDate")]
        public DateTime TransactionDate { get; set; }

        public override string ToString()
        {
            return TransactionDate.ToString("MM/dd/yyyy h:mm:ss tt") + ": " + Sum + " money withdrawn from vehicle with Id='" + VehicleId + "'";
        }
    }
    public class TopUpVehicle
    {
        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; set; }
        [JsonPropertyName("Sum")]
        public decimal? Sum { get; set; }
        public TopUpVehicle(string id, decimal sum)
        {
            VehicleId = id;
            Sum = sum;
        }

    }
}
