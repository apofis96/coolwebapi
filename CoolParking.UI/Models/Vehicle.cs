﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CoolParking.UI.Models
{
    public class Vehicle
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("vehicleType")]
        public int? VehicleType { get; set; }
        [JsonPropertyName("balance")]
        public decimal? Balance { get; set; }
        public Vehicle()
        { }
        public Vehicle(string id, int type, decimal balance)
        {
            Id = id;
            VehicleType = type;
            Balance = balance;
        }
        public override string ToString()
        {
            return Id + " -=- " + (VehicleTypes)VehicleType + " -=- " + "Balance: " + Balance ;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random r = new Random();
            string randId = GenerateAlef() + "-";
            for (int i = 0; i < 4; i++)
            {
                randId += r.Next(0, 9);
            }
            randId += "-" + GenerateAlef();
            return randId;
            string GenerateAlef()
            {
                string str = "";
                for (int i = 0; i < 2; i++)
                {
                    str += Convert.ToChar(r.Next(65, 90)).ToString();
                }
                return str;
            }
        }
    }
    public enum VehicleTypes
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}
