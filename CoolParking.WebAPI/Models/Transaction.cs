﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Models
{
    public class Transaction
    {
        public Transaction(string vehicleId, decimal sum, DateTime date)
        {
            VehicleId = vehicleId;
            Sum = sum;
            TransactionDate = date;
        }
        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; set; }
        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }
        [JsonPropertyName("transactionDate")]
        public DateTime TransactionDate { get; set; }
    }
    public class TopUpVehicle
    {
        [JsonPropertyName("vehicleId")]
        [Required]
        public string VehicleId { get; set; }
        [JsonPropertyName("Sum")]
        [Required]
        public decimal? Sum { get; set; }
    }
}
