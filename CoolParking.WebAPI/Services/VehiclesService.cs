﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Services
{
    public class VehiclesService : Interfaces.IVehiclesService
    {
        private BL.Interfaces.IParkingService parkingService;
        public VehiclesService(BL.Interfaces.IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        public List<Vehicle> GetAll()
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            foreach (var vh in parkingService.GetVehicles())
            {
                vehicles.Add(new Vehicle { Id = vh.Id, Balance = vh.Balance, VehicleType = (int)vh.VehicleType });
            }
            return vehicles;
        }
        public Vehicle Get(string id)
        {
            if (!BL.Models.Vehicle.ValidateRegistrationPlateNumber(id))
            {
                throw new ArgumentException("Wrong ID format");
            }
            var selectedVehicles = from vehicle in GetAll()
                                   where vehicle.Id == id
                                   select vehicle;
            if (selectedVehicles.Count() == 0)
            {
                throw new InvalidOperationException("Vehicle not found");
            }
            return selectedVehicles.First();
        }
        public Vehicle Post(Vehicle vehicle)
        {
            parkingService.AddVehicle(new BL.Models.Vehicle(vehicle.Id, (BL.Models.VehicleType)vehicle.VehicleType, vehicle.Balance.GetValueOrDefault()));
            return vehicle;
        }
        public void Delete(string id)
        {
            if (!BL.Models.Vehicle.ValidateRegistrationPlateNumber(id))
            {
                throw new InvalidOperationException("Wrong ID format");
            }
            parkingService.RemoveVehicle(id);
        }
    }
}
