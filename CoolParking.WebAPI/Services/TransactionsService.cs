﻿using System;
using System.Collections.Generic;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Services
{
    public class TransactionsService : Interfaces.ITransactionsService
    {
        private BL.Interfaces.IParkingService parkingService;
        public TransactionsService(BL.Interfaces.IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        public List<Transaction> GetLast()
        {
            List<Transaction> transactions = new List<Transaction>();
            foreach (BL.Models.TransactionInfo transactionInfo in parkingService.GetLastParkingTransactions())
            {
                transactions.Add(new Transaction(transactionInfo.VehicalId, transactionInfo.Sum, transactionInfo.Date));
            }
            return transactions;
        }
        public string GetAll()
        {
            return parkingService.ReadFromLog();
        }
        public Vehicle TopUp(TopUpVehicle vehicle)
        {
            if (!BL.Models.Vehicle.ValidateRegistrationPlateNumber(vehicle.VehicleId))
            {
                throw new ArgumentException("Wrong ID format");
            }
            try
            {
                parkingService.TopUpVehicle(vehicle.VehicleId, vehicle.Sum.GetValueOrDefault());
            }
            catch (ArgumentException e)
            {
                if (e.Message == "Vehicle with this Id not parked")
                {
                    throw new InvalidOperationException(e.Message);
                }
                throw;
            }
            return new VehiclesService(parkingService).Get(vehicle.VehicleId);
        }
    }
}
