﻿namespace CoolParking.WebAPI.Services
{
    public class ParkingService : Interfaces.IParkingService
    {
        private BL.Interfaces.IParkingService parkingService;
        public ParkingService(BL.Interfaces.IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        public decimal GetBalance()
        {
            return parkingService.GetBalance();
        }
        public int GetCapacity()
        {
            return parkingService.GetCapacity();
        }
        public int GetFreePlaces()
        {
            return parkingService.GetFreePlaces();
        }
    }
}
