using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Services;
using System.IO;
using System.Reflection;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().ConfigureApiBehaviorOptions(options => options.SuppressModelStateInvalidFilter = true);
            BL.Services.TimerService withdrawTimer = new BL.Services.TimerService(BL.Models.Settings.chargeoffPeriod);
            BL.Services.TimerService logTimer = new BL.Services.TimerService(BL.Models.Settings.loggingPeriod);
            BL.Services.LogService logService = new BL.Services.LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");
            services.AddSingleton<BL.Interfaces.IParkingService>(new BL.Services.ParkingService(withdrawTimer, logTimer, logService));
            services.AddSingleton<IVehiclesService, VehiclesService>();
            services.AddTransient<ITransactionsService, TransactionsService>();
            services.AddTransient<IParkingService, ParkingService>();
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
