﻿using System.Collections.Generic;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IVehiclesService
    {
        List<Vehicle> GetAll();
        Vehicle Get(string id);
        Vehicle Post(Vehicle vehicle);
        void Delete(string id);
    }
}
