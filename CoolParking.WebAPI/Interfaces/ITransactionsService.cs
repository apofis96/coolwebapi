﻿using System.Collections.Generic;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Interfaces
{
    public interface ITransactionsService
    {
        List<Transaction> GetLast();
        string GetAll();
        Vehicle TopUp(TopUpVehicle vehicle);
    }
}
