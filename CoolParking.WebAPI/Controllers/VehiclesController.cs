﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IVehiclesService vehiclesService;
        public VehiclesController(IVehiclesService vehiclesService)
        {
            this.vehiclesService = vehiclesService;
        }
        [HttpGet]
        public ActionResult<List<Vehicle>> GetAll()
        {
            return Ok(vehiclesService.GetAll());
        }
        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id)
        {
            try
            {
                return Ok(vehiclesService.Get(id));
            }
            catch (ArgumentException e)
            {
                return BadRequest(Services.ExceptionService.ConvertToDictionary(e));
            }
            catch (InvalidOperationException e)
            {
                return NotFound(Services.ExceptionService.ConvertToDictionary(e));
            }
        }
        [HttpPost]
        public ActionResult<Vehicle> Post([FromBody] Vehicle vehicle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(Services.ExceptionService.ConvertToDictionary(ModelState));
            }
            try
            {
                return Ok(vehiclesService.Post(vehicle));
            }
            catch (Exception e)
            {
                return BadRequest(Services.ExceptionService.ConvertToDictionary(e));
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            try
            {
                vehiclesService.Delete(id);
                return NoContent();
            }
            catch(InvalidOperationException e)
            {
                return BadRequest(Services.ExceptionService.ConvertToDictionary(e));
            }
            catch (ArgumentException e)
            {
                return NotFound(Services.ExceptionService.ConvertToDictionary(e));
            }
        }
    }
}