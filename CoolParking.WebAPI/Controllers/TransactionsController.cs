﻿using System;
using System.Collections.Generic;
using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private ITransactionsService transactionsService;
        public TransactionsController (ITransactionsService transactionsService)
        {
            this.transactionsService = transactionsService;
        }
        [HttpGet("last")]
        public ActionResult<List<Transaction>> GetLast()
        {
            return Ok(transactionsService.GetLast());
        }
        [HttpGet("all")]
        public ActionResult<string> GetAll()
        {
            try
            {
                return Ok(transactionsService.GetAll());
            }
            catch(InvalidOperationException e)
            {
                return NotFound(Services.ExceptionService.ConvertToDictionary(e));
            }
        }
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUp([FromBody] TopUpVehicle vehicle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(Services.ExceptionService.ConvertToDictionary(ModelState));
            }
            try
            {
                return Ok(transactionsService.TopUp(vehicle));
            }
            catch (ArgumentException e)
            {
                return BadRequest(Services.ExceptionService.ConvertToDictionary(e));
            }
            catch (InvalidOperationException e)
            {
                return NotFound(Services.ExceptionService.ConvertToDictionary(e));
            }
        }
    }
}